<!DOCTYPE html>
<html>
    <head>
        <title>Credit Card Information</title>
        <link href="css/creditCardPage.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
    	<div id="cardForm">
	        <form action="Servlet" method="post" >
	            <label for="cardname">Name on card:</label>
	            <input type="text" name="cardname"/>
	            <br />
	
	            <label for="billaddress">Billing Address:</label>
	            <input type="text" name="billaddress"/>
	            <br />
	
	            <label for="cardtype">Card Type: </label>
	            <select>
	                <option>--Please Select --</option>
	                <option value="Visa">Visa</option>
	                <option value="Discover">Discover</option>
	                <option value="AmericanExpress">AmericanExpress</option>
	                <option value="MasterCard">MasterCard</option>
	            </select>
	            <br />
	            
	            <label for="cardnumber">Card Number:</label>
	            <input type="text" name="cardnumber"/>
	            <br />
	            
	            <label for="carddate">Expiration Date: </label>
	            <input type="text" value="01/2017" name="carddate"/>
	            <br />
	            
	            <input type="submit" value="Submit" name="submit_credit"/>
	        </form>
        </div>
    </body>
</html>
