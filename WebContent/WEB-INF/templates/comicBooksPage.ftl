<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<title>Comic Book Page</title>
</head>
<body>
	<h1 align="center"> Comic Book Page</h1>
	
	<div id="search_stuff">
            <form action="Servlet" method="post" id="search">
                <select name="filter">
                    <option value="all">All</option>
                    <option value="video_games">Video Games</option>
                    <option value="comic_books">Comic Books</option>
                    <option value="action_figures">Action Figures</option>
                </select>
                <input type="text" name="search" size="60">
                <input type="submit" value="submit">
            </form>
        </div>
	        
    <br />
    
    <form action="Servlet" method="post" id="nav">
        <input type ="submit" value ="HOME" name = "home"/>
        <input type="submit" value="ACTION FIGURES" name="action_figures"/>
        <input type="submit" value="COMIC BOOKS" name="comic_books"/>
		<input type="submit" value="VIDEO GAMES" name="video_games"/>
        <input type="submit" value="CART" name="cart" id="cart"/>
	</form>
    
    <hr />
	        
    <form id="sortBy">
        <select name="sort_by">
            <option value="best_selling">Best Selling</option>
            <option value="newest">Newest</option>
            <option value="price_low">Price Low To High</option>
            <option value="price_high">Price High To Low</option>
        </select>
        <input type="submit" value="Submit" />
    </form>
    
    
    <#assign x = 200>
    <#list templist as t>
    	<h3>${t.name} -- ${t.price?string.currency}</h3>
        <img src="${t.image}" height="250"/>
        <!--button value will be the item's id number -->
        <form action="Servlet" method="post">
        	<button type="submit" class="cartBtn" value="${x}" name="cart_btn">Add to Cart</button> <br/>
        </form>
        <p>${t.description}</p>
        <#assign x++>
    </#list>

    <hr />
    
	<footer>GeekCandy.com | Copyright 2017</footer>
</html>
