<!DOCTYPE html>
<html>
    <head>
        <title>Cart Page</title>
        <link href="css/cartPage.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <h1 align="center">Your Cart</h1>
        <!-- ALL PAGES HAVE -->
        <div id="search_stuff">
            <form>
                <select name="filter">
                    <option value="all">All</option>
                    <option value="video_games">Video Games</option>
                    <option value="comic_books">Comic Books</option>
                </select>
                <input type="text" name="search" size="60">
                <input type="submit" value="submit">
            </form>
        </div>
        
        <br />
        
        <form action="Servlet" method="post" id="nav">
	        <a href="finalProjectHomepage.html">HOME</a>
	        <input type="submit" value="ACTION FIGURES" name="action_figures"/>
	        <input type="submit" value="COMIC BOOKS" name="comic_books"/>
			<input type="submit" value="VIDEO GAMES" name="video_games"/>
	        <input type="submit" value="CART" name="cart" id="cart"/>
		</form>
        
        <hr />
        <!-- END ALL PAGES HAVE -->    
        
        <table>
            <tr>
                <th>Item</th>
                <th>Price</th>
                <th>Change</th>
            </tr>
            <#list list as l>
            	<tr>
            		<td>${l.item_name}</td>
            		<td>${l.item_price?string.currency}</td>
            		<td>
            			<form action="Servlet" method="post">
            				<button type="submit" value="${l.item_id}" name="delete">Delete</button>
            			</form>
            		</td>
            	</tr>
            </#list>
            <tr>
            	<td></td>
            	<td>TOTAL: ${totalPrice?string.currency}</td>
            	<td></td>
            </tr>
        </table>
        
        <div id="confirm">
			<form action="Servlet" method="post">
			    <input type="submit" value="Confirm Order" name="confirm_order"/>
			</form>
		</div>
        
        <footer>GeekCandy.com | Copyright 2017</footer>
    </body>
</html>
