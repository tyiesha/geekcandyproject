<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Student Register Page</title>
<link href="css/style1.css" rel="stylesheet" type="text/css">

</head>
<body>
<h3>Please enter the following information: </h3>
 <form action="Servlet" method="post">
    <div class="regcl">
 		<label for="fname">Full Name:</label>
 		<input type="text" name="fname" class="textcl" required autocomplete="off" />
 	</div>
 	<div class="regcl">
 		<label for="email">Email:</label>
 		<input type="email" name="email" class="textcl" required autocomplete="off"/>
 	</div>
 	<div>
 		<label for="address">Address:</label>
 		<input type="text" name="address" class="textcl" required autocomplete="off" />
 	</div>
 	<div>
 		<label for="password">Password:</label>
 		<input type="password" name="password" class="textcl" required autocomplete="off" />
 	</div>
 	<div>
 	<button name="register" value="register" class="btn">Register</button>
 	<button type="reset" name="clear" value="clear" class="btn">Clear</button>
 	</div>
 
 </form>
 	 <form action="Servlet" method="post">
 	 Already a Customer? <button name = "redirect2" value = "redirect2" class="btn">Sign up</a>
 	 </form>
</body>
</html>