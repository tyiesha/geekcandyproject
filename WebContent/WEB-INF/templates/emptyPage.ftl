<!DOCTYPE html>
<html>
    <head>
        <title>No Results</title>
        <link href="css/videoGamesPage.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        
        <h1 align="center">Search Results</h1>
        
        <div id="search_stuff">
            <form action="Servlet" method="post" id="search">
                <select name="filter">
                    <option value="all">All</option>
                    <option value="video_games">Video Games</option>
                    <option value="comic_books">Comic Books</option>
                    <option value="action_figures">Action Figures</option>
                </select>
                <input type="text" name="search" size="60">
                <input type="submit" value="submit">
            </form>
        </div>
        
        <br />
        
        <form action="Servlet" method="post" id="nav">
	        <a href="finalProjectHomepage.html">HOME</a>
	        <input type="submit" value="ACTION FIGURES" name="action_figures"/>
	        <input type="submit" value="COMIC BOOKS" name="comic_books"/>
			<input type="submit" value="VIDEO GAMES" name="video_games"/>
	        <input type="submit" value="CART" name="cart" id="cart"/>
		</form>
       
       <p> No Results Found </p>
        
        <footer>GeekCandy.com | Copyright 2017</footer>
    </body>
</html>