<!--This is the index.html page to create the cover webpage -->
<!DOCTYPE html>
<html background="http://www.hdwallpapers.in/walls/guardian_halo_5_guardians-HD.jpg">
	<div id = wrapperDiv>
		<div id = headerDiv align = "center">
			<head>
				<title> Geek Candy</title>
				
				<style>

html{
    background-image: url(http://www.hdwallpapers.in/walls/guardian_halo_5_guardians-HD.jpg);
}

#wrapperDiv{
    margin: 0;
}
#headerDiv{
  text-decoration: underline;
    padding-bottom: 50px;
}

img{
    display: block;
    margin: 0 auto;
    width: 100%;
    height: 400px;
    
}


#games{
  text-align: left;  
}
#comicBooks{
    text-align: center;
}
#Other{
   text-align: right; 
}

.image{
  text-align: center  
}



#aboutDiv{
    width: 40%;
}



.header{
    font-size: x-large;
    text-decoration: underline;

}


* {box-sizing:border-box}

/* Slideshow container */
.slideshow-container {
    max-width: 1050px;
    position: relative;
    margin: auto;
    height: 40%;

}

.mySlides {
    display: none;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 16px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
}

/* Position the "next button" to the right */

.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}


.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}


/* Caption text */
.text {
  color: red;
  font-size: 28px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor:pointer;
  height: 13px;
  width: 13px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active{
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}


/* vv NAV BAR BEGIN CODE */
#nav input[type=submit], #home {
    font-family: Georgia, serif;
    font-size: 14px;
    background: none;
    border: none;
    text-decoration: none;
    font-weight: bold;
    color: white;
    text-align: center;
    padding: 14px 16px;
}

#nav input[type=submit]:hover, #home:hover {
    background: linear-gradient(to top, #4d4d4d 0%, #525252 100%);
}

#nav {
    background: linear-gradient(to bottom, #525252 0%, #999999 100%);
    overflow: hidden;
    margin: 0;
    padding: 0;
}

#cart {
	float: right;
}
/* ^^ NAV BAR END CODE */
#search_stuff, footer{
 /* ALIGN CENTER */ 
    margin-left: auto;
    margin-right: auto;
    text-align: center;
}
#login{
	float:right;
}
				</style>
				<h1>Welcome to Geek Candy </h1>
			</head>
		</div>
		<form action="Servlet" method="post" id="login">
	
       	</form>
        
        <div id="search_stuff">
            <form action="Servlet" method="post" id="search">
                <select name="filter">
                    <option value="all">All</option>
                    <option value="video_games">Video Games</option>
                    <option value="comic_books">Comic Books</option>
                    <option value="action_figures">Action Figures</option>
                </select>
                <input type="text" name="search" size="60">
                <input type="submit" value="submit">
            </form>
        </div>
        </div>
        <br />
        
        <form action="Servlet" method="post" id="nav">
	        <a id="home" href="finalProjectHomepage.html">HOME</a>
	        <input type="submit" value="ACTION FIGURES" name="action_figures"/>
	        <input type="submit" value="COMIC BOOKS " name="comic_books"/>
			<input type="submit" value="VIDEO GAMES" name="video_games"/>
       		<input type = "submit" value = "Register" name = "signup" id = "login"/>
       		<input type = "submit" value = "Login" name = "signin" id = "login"/>
		</form>
        
        <br />
        
		<div class="slideshow-container">
			<div class="mySlides fade">
				<div class="numbertext">1 / 3</div>
				<img class = "image" src="http://cdn.edgecast.steamstatic.com/steam/apps/311210/header.jpg?t=1479852239">
				<div class="text">Video Games</div>
			</div>
			<div class="mySlides fade">
				<div class="numbertext">2 / 3</div>
				<img class = "image" src="http://dallaslivingonthecheap.com/wp-content/uploads/2013/05/comicbooks.jpg" >
				<div class="text">Comic Books</div>
			</div>
			<div class="mySlides fade">
				<div class="numbertext">3 / 3</div>
				<img class = "image" src="https://i.ytimg.com/vi/4EKA5WYOw30/maxresdefault.jpg" >
				<div class="text">Action Figures</div>
			</div>
			<a class="prev">&#10094;</a>
			<a class="next">&#10095;</a>
		</div>
		<br>
		<div style="text-align:center">
			<span class="dot"></span> 
			<span class="dot"></span> 
			<span class="dot"></span> 
		</div>

            	<script>
            	var slideIndex = 0;
            	showSlides();

            	function showSlides() {
            	    var i;
            	    var slides = document.getElementsByClassName("mySlides");
            	    var dots = document.getElementsByClassName("dot");
            	    for (i = 0; i < slides.length; i++) {
            	       slides[i].style.display = "none";  
            	    }
            	    slideIndex++;
            	    if (slideIndex> slides.length) {slideIndex = 1}    
            	    for (i = 0; i < dots.length; i++) {
            	        dots[i].className = dots[i].className.replace(" active", "");
            	    }
            	    slides[slideIndex-1].style.display = "block";  
            	    dots[slideIndex-1].className += " active";
            	    setTimeout(showSlides, 2000); // Change image every 2 seconds
            	}
            	</script>

</html>