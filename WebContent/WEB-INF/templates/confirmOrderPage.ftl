<!DOCTYPE html>
<html>
    <head>
        <title>GeekCandy</title>
        <link href="css/confirmOrderPage.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        
        <h1 id="title">GeekCandy</h1>
        
        <div id="search_stuff">
            <form action="Servlet" method="post" id="search">
                <select name="filter">
                    <option value="all">All</option>
                    <option value="video_games">Video Games</option>
                    <option value="comic_books">Comic Books</option>
                    <option value="action_figures">Action Figures</option>
                </select>
                <input type="text" name="search" size="60">
                <input type="submit" value="submit">
            </form>
        </div>
        <br />
        <form action="Servlet" method="post" id="nav">
	         <input type ="submit" value ="HOME" name = "home"/>
	        <input type="submit" value="ACTION FIGURES" name="action_figures"/>
	        <input type="submit" value="COMIC BOOKS" name="comic_books"/>
			<input type="submit" value="VIDEO GAMES" name="video_games"/>
		</form>
        
        <hr />
        <div id="message">
            <h1>Order is confirmed.</h1>
            <h2>Thanks for shopping!</h2>
        </div>
        
        <footer>GeekCandy.com | Copyright 2017</footer>
    </body>
</html>
