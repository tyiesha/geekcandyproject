
package edu.uga.cs4300.group15.persistlayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbAccessImpl extends DbAccessConfiguration {
	static Connection con = null;
	//Connects to the database
	public static Connection connect() {
		try {
			Class.forName(DRIVE_NAME);
			con = DriverManager.getConnection(CONNECTION_URL, DB_CONNECTION_USERNAME, DB_CONNECTION_PASSWORD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	} // end of connect
	
	public static ResultSet retrieve (String query) {
		ResultSet rset = null;
		try {
			Statement stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			return rset;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rset;
	}// end of retrieve
	
	public static int create (String query) {
		int valid = 0;
		try {
			Statement stmt = con.createStatement();
			valid = stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return valid;
	}
	
	public static int update (String query) {
		
		return 0;
	}
	
	public static int delete (String query) {
		int valid = 0;
		try {
			Statement stmt = con.createStatement();
			valid = stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return valid;
	}	

	//Closes connection to database
	public static void closeConnection(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} // end of closeConnection

}
