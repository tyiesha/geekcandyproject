package edu.uga.cs4300.group15.persistlayer;

import edu.uga.cs4300.group15.persistlayer.DbAccessImpl;
import edu.uga.cs4300.group15.objectlayer.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LogicPersistImpl {

	public ResultSet findSearch(String filter, String search) {
		String sql = null;
		
		if(filter.equalsIgnoreCase("video_games")){
			sql = "select * from video_games " +
			"where name like '" + search + "';";
			System.out.println(sql);
		}
		
		//If the filter is comic_books
		else if(filter.equalsIgnoreCase("comic_books")){
			sql = "select * from comic_books " +
					"where name like '" + search + "';";
			System.out.println(sql);

		}
		
		else if(filter.equalsIgnoreCase("action_figures")){
			sql = "select * from action_figures " +
					"where name like '" + search + "';";
			System.out.println(sql);

		}
		
		return DbAccessImpl.retrieve(sql);
	}
	public ResultSet findGames() {
		String sql = "select * from video_games";
		System.out.println(sql);
		return DbAccessImpl.retrieve(sql);
	}

	public ResultSet findComics() {
		String sql = "select * from comic_books";
		System.out.println(sql);
		return DbAccessImpl.retrieve(sql);
	}
	
	public ResultSet findActionFigures() {
		String sql = "select * from action_figures";
		System.out.println(sql);
		return DbAccessImpl.retrieve(sql);
	}
	
	public ResultSet findLastUserId() {
		String sql = "select id from users order by id desc limit 1";
		return DbAccessImpl.retrieve(sql);
	}
	
	public int persistUser(User u) {
		String sql = "INSERT INTO users (email,password,fullname,address) VALUES "
				+ "('" + u.getEmail()
				+ "','" + u.getPassword()
				+ "','" + u.getFullname()
				+ "','" + u.getAddress()
				+ "')";
		return DbAccessImpl.create(sql);
	}
	
	public ResultSet findItemInfo(int item_id) {
		String sql = "";
		
		if((item_id >= 100) && (item_id < 200))
		{
			sql = "SELECT * from video_games";
		}
		else if((item_id >= 200) && (item_id < 300))
		{
			sql = "SELECT * from comic_books";
		}
		else
		{
			sql = "SELECT * from action_figures";
		}
		
		return DbAccessImpl.retrieve(sql);
	}
	
	public int deleteItemFromCart(int cart_id, int item_id)
	{
		String sql = "DELETE FROM cart WHERE id=" + cart_id
				+ "AND item_id=" + item_id;
		
		return DbAccessImpl.delete(sql);
	}
	
	public int addItemToCart(int cart_id, int item_id)
	{
		String sql = "INSERT INTO cart(id,item_id)"
				+ "VALUES(" + cart_id +"," + item_id +")";
		
		return DbAccessImpl.create(sql);
	}
	
	
}//LogicPersistImpl
