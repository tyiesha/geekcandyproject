package edu.uga.cs4300.group15.boundarylayer;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.uga.cs4300.group15.boundarylayer.*;
import edu.uga.cs4300.group15.logiclayer.*;
import edu.uga.cs4300.group15.objectlayer.*;
import edu.uga.cs4300.group15.persistlayer.DbAccessImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.SimpleHash;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

/**
 * Servlet implementation class Servlet
 */
@WebServlet("/Servlet")
public class Servlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private String templateDir = "/WEB-INF/templates";
	private LogicImpl logicCtrl = new LogicImpl();
	Configuration cfg = null;
	DefaultObjectWrapperBuilder df = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25);
	SimpleHash root = new SimpleHash(df.build());
	Template template = null;
	
	List<Cart> cartList = new ArrayList<Cart>(); 
	int currentUser; //sets in the userlogin and usercreate methods
	
	//to be able to stay on same page after pressing add to cart button
	boolean onAction = false;
	boolean onComic = false;
	boolean onVideo = false;
	
	//to display total in cartPage
	int totalPrice = 0;
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet() {
        super();
    }//Constructor

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() {
		cfg = new Configuration(Configuration.VERSION_2_3_25);
		cfg.setServletContextForTemplateLoading(getServletContext(), templateDir);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
	}//init
	
	private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		response.setContentType("text/html");
        request.getRequestDispatcher("finalProjectHomepage.html").include(request, response);  ;
        System.out.print("You are successfully logged out!");  
	}
	
	private void showHomePage(HttpServletRequest request, HttpServletResponse response) {
		Template template = null;
		
		try {
			template = cfg.getTemplate("homepage.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	} // end of showHomePage

	private void showSignUpPage(HttpServletRequest request, HttpServletResponse response) {
		Template template = null;
	
		try {
			template = cfg.getTemplate("register.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	} // end of showSignUpPage

	public void runLoginPage(HttpServletRequest request, HttpServletResponse response)
	{
		Template template = null;
		
		try {
			template = cfg.getTemplate("login.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}//runLoginPageTemplate

	public void runVideoGamesTemplate(HttpServletRequest request, HttpServletResponse response)
	{	
		Template template = null;
		
		onAction = false;
		onComic = false;
		onVideo = true;
		
		try {
			template = cfg.getTemplate("videoGamesPage.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}//runVideoGamesTemplate
	
	public void runGenericTemplate(HttpServletRequest request, HttpServletResponse response)
	{	
		Template template = null;
		
		onAction = false;
		onComic = false;
		onVideo = false;
		
		try {
			template = cfg.getTemplate("genericPage.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}//runGenericTemplate
	
	/*
	 * 
	 */
	public void runComicBooksTemplate(HttpServletRequest request, HttpServletResponse response)
	{	
		Template template = null;
		
		onAction = false;
		onComic = true;
		onVideo = false;
		
		try {
			template = cfg.getTemplate("comicBooksPage.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}//runComicBooksTemplate
	
	/*
	 * 
	 */
	public void runActionFiguresTemplate(HttpServletRequest request, HttpServletResponse response)
	{	
		Template template = null;
		
		onAction = true;
		onComic = false;
		onVideo = false;
		
		try {
			template = cfg.getTemplate("actionFiguresPage.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}//runActionFiguresTemplate
	
	/*
	 * 
	 */
	public void runCartPageTemplate(HttpServletRequest request, HttpServletResponse response)
	{	
		Template template = null;
		
		try {
			template = cfg.getTemplate("cartPage.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}//runCartPageTemplate
	
	public void runCreditCardPage(HttpServletRequest request, HttpServletResponse response)
	{
		Template template = null;
		
		try {
			template = cfg.getTemplate("creditCardPage.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
		
	}//runCreditCardPage
	public void LoginError(HttpServletRequest request, HttpServletResponse response)
	{
		Template template = null;
		
		try {
			template = cfg.getTemplate("loginerror.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}//LoginError
	
	public void runConfirmOrderPage(HttpServletRequest request, HttpServletResponse response)
	{
		Template template = null;
		
		try {
			template = cfg.getTemplate("confirmOrderPage.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}//runConfirmOrderPage
	
	/*
	 * Method to create a new user from the register page and also creates their cart.
	 */
	public void createUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{	
		int id = 0;
		String fullname = request.getParameter("fname");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String password = request.getParameter("password");
		
		
		ResultSet rs = logicCtrl.findLastUserId();
		
		//to get id from database
		try {
			
			while(rs.next())
			{
				id = rs.getInt(1)+1;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		currentUser = id;
		
		logicCtrl.insertUser(id, email, fullname, password, address);
		
		//create user's cart
		String cartQuery = "INSERT INTO cart(id) VALUES ('"+ id+ "');";
		DbAccessImpl.create(cartQuery);
		
	}//createUser
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logicCtrl.connectToDatabase();
		String redirect2 = request.getParameter("redirect2");
		String redirect = request.getParameter("redirect");
		String SignIn = request.getParameter("SignIn");
	
		//register
		String signup = request.getParameter("signup");
		String signin = request.getParameter("signin");
		String registerButton = request.getParameter("register");
		
		//nav bar
	    String homePage = request.getParameter("home");
		String videoGamesPage = request.getParameter("video_games");
		String comicBooksPage = request.getParameter("comic_books");
		String actionFiguresPage = request.getParameter("action_figures");
		String cartPage = request.getParameter("cart");
		String logout = request.getParameter("logout");
		
		//add to cart button
		String addToCart = request.getParameter("cart_btn");
		
		//delete item from cart
		String deleteItem = request.getParameter("delete");
		
		//credit card info page
		String submitCredit = request.getParameter("submit_credit");
		
		//from cart page
		String confirmOrder = request.getParameter("confirm_order");
		
		//HANDLING DROPDOWN LIST
		String sortDropdown = request.getParameter("sort_by");
		
		if(sortDropdown != null)
		{
			if(sortDropdown.equalsIgnoreCase("best_selling"))
			{	
				if(onVideo)
				{
					String query = "select * from video_games ORDER BY bestselling_order";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<VideoGame> vglist = new ArrayList<VideoGame>();
					
					try {
						
						while(rs.next())
						{
							VideoGame vg = new VideoGame();
							
							vg.setId(rs.getInt("id"));
							vg.setName(rs.getString("name"));
							vg.setPrice(rs.getInt("price"));
							vg.setDescription(rs.getString("description"));
							vg.setBestselling_order(rs.getInt("bestselling_order"));
							vg.setImage(rs.getString("image"));
							vg.setConsole(rs.getString("console"));
							
							vglist.add(vg);
							
							root.put("templist", vglist);
						}
						
						runVideoGamesTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				else if(onComic)
				{
					String query = "select * from comic_books ORDER BY bestselling_order";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<ComicBook> cblist = new ArrayList<ComicBook>();
					
					try {
						
						while(rs.next())
						{
							ComicBook cb = new ComicBook();
							
							cb.setId(rs.getInt("id"));
							cb.setName(rs.getString("name"));
							cb.setPrice(rs.getInt("price"));
							cb.setDescription(rs.getString("description"));
							cb.setBestselling_order(rs.getInt("bestselling_order"));
							cb.setImage(rs.getString("image"));
							
							cblist.add(cb);
							
							root.put("templist", cblist);
						}
						
						runComicBooksTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
				else if(onAction)
				{
					String query = "select * from action_figures ORDER BY bestselling_order";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<ActionFigures> aflist = new ArrayList<ActionFigures>();
					
					try {
						
						while(rs.next())
						{
							ActionFigures af = new ActionFigures();
							
							af.setId(rs.getInt("id"));
							af.setName(rs.getString("name"));
							af.setPrice(rs.getInt("price"));
							af.setDescription(rs.getString("description"));
							af.setBestselling_order(rs.getInt("bestselling_order"));
							af.setImage(rs.getString("image"));
							
							aflist.add(af);
							
							root.put("templist", aflist);
						}
						
						runActionFiguresTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
				
			}
			else if(sortDropdown.equalsIgnoreCase("newest"))
			{
				if(onVideo)
				{
					String query = "select * from video_games ORDER BY id DESC";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<VideoGame> vglist = new ArrayList<VideoGame>();
					
					try {
						
						while(rs.next())
						{
							VideoGame vg = new VideoGame();
							
							vg.setId(rs.getInt("id"));
							vg.setName(rs.getString("name"));
							vg.setPrice(rs.getInt("price"));
							vg.setDescription(rs.getString("description"));
							vg.setBestselling_order(rs.getInt("bestselling_order"));
							vg.setImage(rs.getString("image"));
							vg.setConsole(rs.getString("console"));
							
							vglist.add(vg);
							
							root.put("templist", vglist);
						}
						
						runVideoGamesTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				else if(onComic)
				{
					String query = "select * from comic_books ORDER BY id DESC";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<ComicBook> cblist = new ArrayList<ComicBook>();
					
					try {
						
						while(rs.next())
						{
							ComicBook cb = new ComicBook();
							
							cb.setId(rs.getInt("id"));
							cb.setName(rs.getString("name"));
							cb.setPrice(rs.getInt("price"));
							cb.setDescription(rs.getString("description"));
							cb.setBestselling_order(rs.getInt("bestselling_order"));
							cb.setImage(rs.getString("image"));
							
							cblist.add(cb);
							
							root.put("templist", cblist);
						}
						
						runComicBooksTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
				else if(onAction)
				{
					String query = "select * from action_figures ORDER BY id DESC";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<ActionFigures> aflist = new ArrayList<ActionFigures>();
					
					try {
						
						while(rs.next())
						{
							ActionFigures af = new ActionFigures();
							
							af.setId(rs.getInt("id"));
							af.setName(rs.getString("name"));
							af.setPrice(rs.getInt("price"));
							af.setDescription(rs.getString("description"));
							af.setBestselling_order(rs.getInt("bestselling_order"));
							af.setImage(rs.getString("image"));
							
							aflist.add(af);
							
							root.put("templist", aflist);
						}
						
						runActionFiguresTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
				
			}
			else if(sortDropdown.equalsIgnoreCase("price_low"))
			{
				if(onVideo)
				{
					String query = "select * from video_games ORDER BY price";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<VideoGame> vglist = new ArrayList<VideoGame>();
					
					try {
						
						while(rs.next())
						{
							VideoGame vg = new VideoGame();
							
							vg.setId(rs.getInt("id"));
							vg.setName(rs.getString("name"));
							vg.setPrice(rs.getInt("price"));
							vg.setDescription(rs.getString("description"));
							vg.setBestselling_order(rs.getInt("bestselling_order"));
							vg.setImage(rs.getString("image"));
							vg.setConsole(rs.getString("console"));
							
							vglist.add(vg);
							
							root.put("templist", vglist);
						}
						
						runVideoGamesTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				else if(onComic)
				{
					String query = "select * from comic_books ORDER BY price";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<ComicBook> cblist = new ArrayList<ComicBook>();
					
					try {
						
						while(rs.next())
						{
							ComicBook cb = new ComicBook();
							
							cb.setId(rs.getInt("id"));
							cb.setName(rs.getString("name"));
							cb.setPrice(rs.getInt("price"));
							cb.setDescription(rs.getString("description"));
							cb.setBestselling_order(rs.getInt("bestselling_order"));
							cb.setImage(rs.getString("image"));
							
							cblist.add(cb);
							
							root.put("templist", cblist);
						}
						
						runComicBooksTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
				else if(onAction)
				{
					String query = "select * from action_figures ORDER BY price";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<ActionFigures> aflist = new ArrayList<ActionFigures>();
					
					try {
						
						while(rs.next())
						{
							ActionFigures af = new ActionFigures();
							
							af.setId(rs.getInt("id"));
							af.setName(rs.getString("name"));
							af.setPrice(rs.getInt("price"));
							af.setDescription(rs.getString("description"));
							af.setBestselling_order(rs.getInt("bestselling_order"));
							af.setImage(rs.getString("image"));
							
							aflist.add(af);
							
							root.put("templist", aflist);
						}
						
						runActionFiguresTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
				
			}
			else if(sortDropdown.equalsIgnoreCase("price_high"))
			{
				if(onVideo)
				{
					String query = "select * from video_games ORDER BY price DESC";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<VideoGame> vglist = new ArrayList<VideoGame>();
					
					try {
						
						while(rs.next())
						{
							VideoGame vg = new VideoGame();
							
							vg.setId(rs.getInt("id"));
							vg.setName(rs.getString("name"));
							vg.setPrice(rs.getInt("price"));
							vg.setDescription(rs.getString("description"));
							vg.setBestselling_order(rs.getInt("bestselling_order"));
							vg.setImage(rs.getString("image"));
							vg.setConsole(rs.getString("console"));
							
							vglist.add(vg);
							
							root.put("templist", vglist);
						}
						
						runVideoGamesTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
				else if(onComic)
				{
					String query = "select * from comic_books ORDER BY price DESC";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<ComicBook> cblist = new ArrayList<ComicBook>();
					
					try {
						
						while(rs.next())
						{
							ComicBook cb = new ComicBook();
							
							cb.setId(rs.getInt("id"));
							cb.setName(rs.getString("name"));
							cb.setPrice(rs.getInt("price"));
							cb.setDescription(rs.getString("description"));
							cb.setBestselling_order(rs.getInt("bestselling_order"));
							cb.setImage(rs.getString("image"));
							
							cblist.add(cb);
							
							root.put("templist", cblist);
						}
						
						runComicBooksTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
				else if(onAction)
				{
					String query = "select * from action_figures ORDER BY price DESC";
					ResultSet rs = DbAccessImpl.retrieve(query);
					
					List<ActionFigures> aflist = new ArrayList<ActionFigures>();
					
					try {
						
						while(rs.next())
						{
							ActionFigures af = new ActionFigures();
							
							af.setId(rs.getInt("id"));
							af.setName(rs.getString("name"));
							af.setPrice(rs.getInt("price"));
							af.setDescription(rs.getString("description"));
							af.setBestselling_order(rs.getInt("bestselling_order"));
							af.setImage(rs.getString("image"));
							
							aflist.add(af);
							
							root.put("templist", aflist);
						}
						
						runActionFiguresTemplate(request, response);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
				
			}
			
		}
		
		
		if(signin != null)
		{
			runLoginPage(request, response);
		}
		if(redirect2 !=null)
		{
			runLoginPage(request, response);
		}
		if(redirect !=null)
		{
			showSignUpPage(request, response);
		}
		
		if(signup != null)
		{
			showSignUpPage(request, response);
		}
		
		if(registerButton != null)
		{
			createUser(request, response);
			//go to login page
			runLoginPage(request, response);
		}
		if (SignIn != null)
		{
			//login
			System.out.println("test");
			response.setContentType("text/html");
			String msg = " ";
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			HttpSession session=request.getSession();
		    session.setAttribute("email", email);
		    root.put("email",email);
		    root.put("password",password);
		
			try{
				
			  Class.forName("com.mysql.jdbc.Driver");
			  Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/group_project","root","root");  
			  String strQuery = "select * from users WHERE email='"
			  + email + "' and password='" + password + "'";
				        Statement st = (Statement) con.createStatement();
				        ResultSet rs = st.executeQuery(strQuery);
				        if (rs.next()) { 
				        	showHomePage(request, response);
							currentUser = rs.getInt(1);
				            msg = "HELLO " + email + "! Your login is SUCESSFULL";
				            st.close();
				            con.close();
					        rs.close();
				        } else{
				        	LoginError(request, response);
				            msg = "HELLO " + email + "! Your login is UNSUCESSFULL";
				            con.close();
					        rs.close();
					        st.close();
				        }
				        System.out.println("test :" + msg);
				    } catch (Exception e) {
				        e.printStackTrace();
				    }
		}
		if(logout != null)
		{ 
			logout(request, response);
		}
		
		if(homePage != null)
		{
			showHomePage(request, response);
		}
		//searchBar
		String search = request.getParameter("search");
		String filter = request.getParameter("filter");
		if(search != null){
			
			int empty = 0;
			search = search + "%";
			System.out.println(search);
			//If the filter is all
			if(filter.equalsIgnoreCase("all")){
				filter = "video_games";
				ResultSet rs = logicCtrl.findSearch(filter, search);
				
				List<VideoGame> vglist = new ArrayList<VideoGame>();
				
				try {
					if (!rs.next()) {
						empty++;
						root.put("templist0", vglist);
					}
					rs.beforeFirst();
					while(rs.next())
					{
						VideoGame vg = new VideoGame();
						
						vg.setId(rs.getInt("id"));
						vg.setName(rs.getString("name"));
						vg.setPrice(rs.getInt("price"));
						vg.setDescription(rs.getString("description"));
						vg.setBestselling_order(rs.getInt("bestselling_order"));
						vg.setImage(rs.getString("image"));
						vg.setConsole(rs.getString("console"));
						
						vglist.add(vg);
						
						root.put("templist0", vglist);
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				filter = "comic_books";
				rs = logicCtrl.findSearch(filter, search);
				List<ComicBook> cblist = new ArrayList<ComicBook>();
				
				try {
					if (!rs.next()) {
						empty++;
						root.put("templist1", cblist);
					}
					rs.beforeFirst();
					while(rs.next())
					{
						ComicBook cb = new ComicBook();
						
						cb.setId(rs.getInt("id"));
						cb.setName(rs.getString("name"));
						cb.setPrice(rs.getInt("price"));
						cb.setDescription(rs.getString("description"));
						cb.setBestselling_order(rs.getInt("bestselling_order"));
						cb.setImage(rs.getString("image"));
						
						cblist.add(cb);
						
						root.put("templist1", cblist);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				filter = "action_figures";
				rs = logicCtrl.findSearch(filter, search);
				
				List<ActionFigures> aflist = new ArrayList<ActionFigures>();
				
				try {
					if (!rs.next()) {
						empty++;
						root.put("templist2", aflist);
						if (empty == 3){
							runEmptyTemplate(request, response);
							return;
						}
					}
					rs.beforeFirst();
					while(rs.next())
					{
						ActionFigures af = new ActionFigures();
						
						af.setId(rs.getInt("id"));
						af.setName(rs.getString("name"));
						af.setPrice(rs.getInt("price"));
						af.setDescription(rs.getString("description"));
						af.setBestselling_order(rs.getInt("bestselling_order"));
						af.setImage(rs.getString("image"));
						
						aflist.add(af);
						
						root.put("templist2", aflist);
					}
					runGenericTemplate(request, response);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				
				
			}
			
			//If the filter is video_games
			else if(filter.equalsIgnoreCase("video_games")){
				ResultSet rs = logicCtrl.findSearch(filter, search);
				
				List<VideoGame> vglist = new ArrayList<VideoGame>();
				
				try {
					if (!rs.next()) {
						runEmptyTemplate(request, response);
						return;
					}
					rs.beforeFirst();
					while(rs.next())
					{
						VideoGame vg = new VideoGame();
						
						vg.setId(rs.getInt("id"));
						vg.setName(rs.getString("name"));
						vg.setPrice(rs.getInt("price"));
						vg.setDescription(rs.getString("description"));
						vg.setBestselling_order(rs.getInt("bestselling_order"));
						vg.setImage(rs.getString("image"));
						vg.setConsole(rs.getString("console"));
						
						vglist.add(vg);
						
						root.put("templist", vglist);
					}
					
					runVideoGamesTemplate(request, response);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			//If the filter is comic_books
			else if(filter.equalsIgnoreCase("comic_books")){
				
				ResultSet rs = logicCtrl.findSearch(filter, search);
				
				List<ComicBook> cblist = new ArrayList<ComicBook>();
				
				try {
					if (!rs.next()) {
						runEmptyTemplate(request, response);
						return;
					}
					rs.beforeFirst();
					while(rs.next())
					{
						ComicBook cb = new ComicBook();
						
						cb.setId(rs.getInt("id"));
						cb.setName(rs.getString("name"));
						cb.setPrice(rs.getInt("price"));
						cb.setDescription(rs.getString("description"));
						cb.setBestselling_order(rs.getInt("bestselling_order"));
						cb.setImage(rs.getString("image"));
						
						cblist.add(cb);
						
						root.put("templist", cblist);
					}
					
					runComicBooksTemplate(request, response);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			//If the filter is action_figures
			else if(filter.equalsIgnoreCase("action_figures")){
				ResultSet rs = logicCtrl.findSearch(filter, search);
				
				List<ActionFigures> aflist = new ArrayList<ActionFigures>();
				
				try {
					if (!rs.next()) {
						runEmptyTemplate(request, response);
						return;
					}
					rs.beforeFirst();
					while(rs.next())
					{
						ActionFigures af = new ActionFigures();
						
						af.setId(rs.getInt("id"));
						af.setName(rs.getString("name"));
						af.setPrice(rs.getInt("price"));
						af.setDescription(rs.getString("description"));
						af.setBestselling_order(rs.getInt("bestselling_order"));
						af.setImage(rs.getString("image"));
						
						aflist.add(af);
						
						root.put("templist", aflist);
					}
					
					runActionFiguresTemplate(request, response);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			search = null;
		}
		
		//NAV BAR
		if(videoGamesPage != null)
		{
			ResultSet rs = logicCtrl.findGames();
			
			List<VideoGame> vglist = new ArrayList<VideoGame>();
			
			try {
				
				while(rs.next())
				{
					VideoGame vg = new VideoGame();
					
					vg.setId(rs.getInt("id"));
					vg.setName(rs.getString("name"));
					vg.setPrice(rs.getInt("price"));
					vg.setDescription(rs.getString("description"));
					vg.setBestselling_order(rs.getInt("bestselling_order"));
					vg.setImage(rs.getString("image"));
					vg.setConsole(rs.getString("console"));
					
					vglist.add(vg);
					
					root.put("templist", vglist);
				}
				
				runVideoGamesTemplate(request, response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		if(comicBooksPage != null)
		{
			ResultSet rs = logicCtrl.findComics();
			
			List<ComicBook> cblist = new ArrayList<ComicBook>();
			
			try {
				
				while(rs.next())
				{
					ComicBook cb = new ComicBook();
					
					cb.setId(rs.getInt("id"));
					cb.setName(rs.getString("name"));
					cb.setPrice(rs.getInt("price"));
					cb.setDescription(rs.getString("description"));
					cb.setBestselling_order(rs.getInt("bestselling_order"));
					cb.setImage(rs.getString("image"));
					
					cblist.add(cb);
					
					root.put("templist", cblist);
				}
				
				runComicBooksTemplate(request, response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		if(actionFiguresPage != null)
		{
			ResultSet rs = logicCtrl.findActionFigures();
			
			List<ActionFigures> aflist = new ArrayList<ActionFigures>();
			
			try {
				
				while(rs.next())
				{
					ActionFigures af = new ActionFigures();
					
					af.setId(rs.getInt("id"));
					af.setName(rs.getString("name"));
					af.setPrice(rs.getInt("price"));
					af.setDescription(rs.getString("description"));
					af.setBestselling_order(rs.getInt("bestselling_order"));
					af.setImage(rs.getString("image"));
					
					aflist.add(af);
					
					root.put("templist", aflist);
				}
				
				runActionFiguresTemplate(request, response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		if(cartPage != null)
		{
			
			for(int i = 0; i < cartList.size(); i++)
			{
				Cart add = cartList.get(i);
				totalPrice += add.getItem_price();
			}
			
			root.put("totalPrice", totalPrice);
			root.put("list", cartList);	
			runCartPageTemplate(request, response);
		}
		
		if(addToCart != null)
		{
			int itemNum = Integer.parseInt(addToCart);
			
			Cart cart = new Cart();
			
			cart.setId(currentUser);
			cart.setItem_id(itemNum);
			
			//add to cart table
			logicCtrl.addItemToCart(currentUser, itemNum);
			
			ResultSet rs = logicCtrl.findItemInfo(itemNum);
			
			try {
				
				while(rs.next())
				{
					cart.setItem_name(rs.getString("name"));
					cart.setItem_price(rs.getInt("price"));
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			cartList.add(cart);
			
			//to stay on same page
			if(onAction)
			{
				runActionFiguresTemplate(request, response);
			}
			else if(onComic)
			{
				runComicBooksTemplate(request, response);
			}
			else if(onVideo)
			{
				runVideoGamesTemplate(request, response);
			}
			
		}
		
		if(deleteItem != null)
		{
			int itemNum = Integer.parseInt(deleteItem);
			
			//delete item from cart table
			logicCtrl.deleteItemFromCart(currentUser, itemNum);
			
			//delete item from cartList
			for(int i = 0; i < cartList.size(); i++)
			{
				Cart add = cartList.get(i);
				
				if((add.getId() == currentUser) && (add.getItem_id() == itemNum))
				{
					totalPrice -= add.getItem_price();
					cartList.remove(add);
				}
				
			}
			
			root.put("totalPrice", totalPrice);
			root.put("list", cartList);	
			runCartPageTemplate(request, response);
		}
		
		if(confirmOrder != null)
		{
			runCreditCardPage(request, response);
		}
		
		if(submitCredit != null)
		{
			runConfirmOrderPage(request, response);
		}
		
		
		
		logicCtrl.disconnectFromDatabase();
	}//doGet

	private void runEmptyTemplate(HttpServletRequest request, HttpServletResponse response) {
		
		Template template = null;
		
		onAction = false;
		onComic = false;
		onVideo = false;
		
		try {
			template = cfg.getTemplate("emptyPage.ftl");
			response.setContentType("text/html");
			Writer out = response.getWriter();
			template.process(root, out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}//doPost
}//Servlet

