package edu.uga.cs4300.group15.objectlayer;

public class Cart {
	
	private int id; //same as user.id
	private int item_id;
	private String item_name;
	private int item_price;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getItem_id() {
		return item_id;
	}
	
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	
	public String getItem_name() {
		return item_name;
	}
	
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	
	public int getItem_price() {
		return item_price;
	}
	
	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}
	
}//Cart
