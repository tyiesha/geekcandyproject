package edu.uga.cs4300.group15.objectlayer;

public class User {

	private int id;
	private String email;
	private String password;
	private String fullname;
	private String address;
	
	/*
	 * 
	 */
	public User(int id, String email, String fullname, String password, String address)
	{
		this.id = id;
		this.email = email;
		this.fullname = fullname;
		this.password = password;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
}//User
