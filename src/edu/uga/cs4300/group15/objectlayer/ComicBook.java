package edu.uga.cs4300.group15.objectlayer;

public class ComicBook {

	private int id;
	private String name;
	private int price;
	private String description;
	private int bestselling_order;
	private String image;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getBestselling_order() {
		return bestselling_order;
	}
	
	public void setBestselling_order(int bestselling_order) {
		this.bestselling_order = bestselling_order;
	}
	
	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	
	
}//ComicBook
