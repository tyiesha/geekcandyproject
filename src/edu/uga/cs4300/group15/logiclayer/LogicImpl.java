package edu.uga.cs4300.group15.logiclayer;

import edu.uga.cs4300.group15.objectlayer.User;
import edu.uga.cs4300.group15.persistlayer.*;


import java.sql.Connection;
import java.sql.ResultSet;

public class LogicImpl {
	LogicPersistImpl logicPersist;
	Connection con;

	public LogicImpl() {
		logicPersist = new LogicPersistImpl();
	}
	
	public ResultSet findSearch(String filter, String search) {
		return logicPersist.findSearch(filter, search);	
	}
	
	public ResultSet findGames() {
		return logicPersist.findGames();
	}
	
	public ResultSet findComics() {
		return logicPersist.findComics();
	}
	
	public ResultSet findActionFigures() {
		return logicPersist.findActionFigures();
	}
	
	public ResultSet findLastUserId() {
		return logicPersist.findLastUserId();
	}
	
	public int insertUser(int id, String email, String fullname, String password, String address) {
		User u = new User(id, email, fullname, password, address);
		return logicPersist.persistUser(u);
	}
	
	public ResultSet findItemInfo(int item_id){
		return logicPersist.findItemInfo(item_id);
	}
	
	public int deleteItemFromCart(int cart_id, int item_id)
	{
		return logicPersist.deleteItemFromCart(cart_id, item_id);
	}
	
	public int addItemToCart(int cart_id, int item_id)
	{
		return logicPersist.addItemToCart(cart_id, item_id);
	}
	
	public void connectToDatabase(){
		System.out.println("Connecting to database...");
		con = DbAccessImpl.connect();
		System.out.println("Connected to database successfully.");
	}

	//Finished
	public void disconnectFromDatabase(){
		DbAccessImpl.closeConnection(con);
	}


}//LogicImpl
