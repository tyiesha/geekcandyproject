-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: group_project
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_figures`
--

DROP TABLE IF EXISTS `action_figures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_figures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `bestselling_order` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_figures`
--

LOCK TABLES `action_figures` WRITE;
/*!40000 ALTER TABLE `action_figures` DISABLE KEYS */;
INSERT INTO `action_figures` VALUES (300,'Batman Arkham Asylum: The Joker','a',8,'Batman Arkham Asylum The Joker Pop Vinyl Figure from Funko! Perfect for any fan of the Batman comics! Figure stands 3 3/4 inches.',2,'https://images-na.ssl-images-amazon.com/images/I/41UACwQHlSL.jpg'),(301,'The Hulk','a',20,'Hulk is a mountain of sheer destructive force with strength beyond anything the world has ever seen! Towering in at 12 inches tall, this Hulk figure is a bulked-up, gamma-powered force to be reckoned with!',4,'https://images-na.ssl-images-amazon.com/images/I/81PIOmrdDcL._SL1500_.jpg'),(302,'Assassins Creed Series: Ezio Auditore Da Firenze','a',40,'From the videogame Assassins Creed Revelations comes this highly detailed action figure. It stands approx. 18 cm tall and comes with accessories in a blister packaging',1,'https://images-na.ssl-images-amazon.com/images/I/61VqrqC4SoL._SL1001_.jpg'),(303,'Guardians of the Galaxy 2: Baby Groot','a',8,'From Guardians of the Galaxy 2, Toddler Groot, as a stylized POP vinyl from Funko! Figure stands 3 3/4 inches and comes in a window display box.',3,'https://images-na.ssl-images-amazon.com/images/I/71Tk9o0lyKL._SL1500_.jpg'),(304,'Assassins Creed Series: Connor','a',30,'Our Connor figure depicts this secret hero of the revolution in his hooded Colonial Assassin robes, as seen on the cover of Assassins Creed III. Figure has 27 points of articulation and comes with an Assassin tomahawk, bow, a quiver of arrows and a code to unlock exclusive Assassins Creed video game content',5,'https://images-na.ssl-images-amazon.com/images/I/710tN4vX1jL._SL1500_.jpg'),(305,'Minescraft Diamond Steve','a',5,'From the hit video game, Minecraft, bring home the Diamond Armor Steve action figure pack. This action figure pack features a 2.75 in articulated Diamond Steve figure in the most durable armor including a removable helmet, diamond block and a sword that fits in his hand. Collect all Series No. 2 Minecraft action figures!',6,'https://images-na.ssl-images-amazon.com/images/I/71nQ2ZkoZfL._SL1500_.jpg');
/*!40000 ALTER TABLE `action_figures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (4,NULL);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comic_books`
--

DROP TABLE IF EXISTS `comic_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comic_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `bestselling_order` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comic_books`
--

LOCK TABLES `comic_books` WRITE;
/*!40000 ALTER TABLE `comic_books` DISABLE KEYS */;
INSERT INTO `comic_books` VALUES (200,'Batman No. 1','c',20,'Great villains beget great heroes, and this first full-on Batman comic features The Joker, Hugo Strange and Cat Woman. (Spoiler alert: The Joker dies.) Batman was introduced two years earlier in Detective Comics #27 in a six-page shorty that Isabella says essentially plagiarized The Spirit.',1,'http://vignette4.wikia.nocookie.net/batman/images/3/32/Batmanno1.jpg/revision/latest?cb=20101020172136'),(201,'Incredible Hulk','c',13,'Tapping into nuclear angst, Jack Kirby and Stan Lee introduce scientist Bruce Banner in their origins story about a creature created when a gamma radiation bomb goes bad.',3,'http://vignette4.wikia.nocookie.net/marveldatabase/images/f/fa/Incredible_Hulk_Vol_1_1.jpg/revision/latest?cb=20070314200016'),(202,'Weird Fantasy No. 17','c',19,'Artists Joe Orlando and Wally Wood illustrated Weird adaptations of Ray Bradburys stories. The legendary sci-fi author cut a deal with EC comics to produce illustrated versions of his tales after catching several uncredited swipes of his material. There Will Come Soft Rains, like many mid-century comic boo stories, was inspired by the fear of imminent nuclear annihilation.',4,'https://marswillsendnomore.files.wordpress.com/2011/12/weird-fantasy-17-fc.jpg?w=600'),(203,'Wolverine No. 1-4','c',15,'In Frank Millers take on the saber-clawed mutant, Wolverine battles ninjas in Japan and first utters his memorable tagline: Im the best there is at what I do , but what I do isnt very nice.',2,'https://www.wired.com/images_blogs/underwire/2009/12/1982-wolverine-1-4.jpg'),(204,'Batman: A Death in the Family','c',12,'Batman readers were allowed to vote on the outcome of the story and they decided that Robin should die! As the second person to assume the role of Batmans sidekick, Jason Todd had a completely different personality than the original Robin. Rash and prone to ignore Batmans instructions, Jason was always quick to act without regard to consequences. In this fatal instance, Robin ignores his mentors warnings when he attempts to take on the Joker by himself and pays the ultimate price. Driven by anger with Superman by his side, Batman seeks his vengeance as he looks to end the Jokers threat forever.',5,'https://images-na.ssl-images-amazon.com/images/I/71hkKVtPj6L.jpg'),(205,'Spiderman: Miles Morales Vol. 1','c',10,'Miles Morales is hitting the big time! Not only is he joining the Marvel Universe, but hes also a card-carrying Avenger, rubbing shoulders with the likes of Iron Man, Thor and Captain America! But how have Miles first eight months been, coming to grips with an All-new, All-Different new York? One thing is the same--nonstop action! Like when Earths Mightiest Heroes all fall, and Miles stands alone against a villain with the power to destroy the universe. Or when the Black Cat tries to get her claws in this new Spider-Man. Then theres Miles toughest foe yet--his grandmother! But his grades might be his biggest challenge...maybe a study session (date?) with fellow Avenger Ms. Marvel might help? Not likely! Dont miss the start of Miles Morales adventures in the Marvel Universe!',6,'https://images-na.ssl-images-amazon.com/images/I/61xo%2BKc2wXL.jpg');
/*!40000 ALTER TABLE `comic_books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'test','123','the test','100 some');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_games`
--

DROP TABLE IF EXISTS `video_games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `bestselling_order` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `console` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_games`
--

LOCK TABLES `video_games` WRITE;
/*!40000 ALTER TABLE `video_games` DISABLE KEYS */;
INSERT INTO `video_games` VALUES (100,'Assassins Creed 2','v',15,'Get ready to plunge into the lush and deadly world of the Italian Renaissance, an era of arts, riches and murderous conspiracy. Assassins Creed II introduces you to Ezio, a new assassin carrying on the deadly lineage of his forebears. Confront an epic tale of power and corruption as you hone your assassins art, wielding weapons and instruments designed by the legendary Leonard da Vinci himself in this gripping and deadly sequel.\n',2,'http://vignette2.wikia.nocookie.net/assassinscreed/images/0/09/AC2coverHighRes.jpg/revision/latest?cb=20120706023159\n','Xbox 360; PS3'),(101,'Battlefield Hardline','v',45,'Get a piece of the action in Battlefield Hardline, a fresh, new take on Battlefield that allows you to live out your cops and criminal fantasy. Combining an emotionally-driven single player story inspired by popular TV crime dramas, and an all-out-war multiplayer on the streets of Los Angeles and Miami, Hardline delivers the most complete FPS on the market.\n',3,'https://upload.wikimedia.org/wikipedia/en/a/aa/Battlefield_Hardline.jpg\n','Xbox One; PS4'),(102,'Grand Theft Auto 5','v',20,'Los Santos: a sprawling sun-soaked metropolis full of self-help gurus, starlets and fading celebrities, once the envy of the Western world, now struggling to stay afloat in an era of economic uncertainty and cheap reality TV. Amidst the turmoil, three very different criminals risk everything in a series of daring and dangerous heists that could set them up for life.\n',1,'https://upload.wikimedia.org/wikipedia/en/a/a5/Grand_Theft_Auto_V.png\n','Xbox 360; Xbox One; PS3; PS4'),(103,'The Legend of Zelda: Breath of the Wild','v',50,'Step into a world of discovery, exploration, and adventure in The Legend of Zelda: Breath of the Wild, a boundary-breaking new game in the acclaimed series. Travel across vast fields, through forests, and to mountain peaks as you discover what has become of the kingdom of Hyrule in this stunning Open-Air Adventure.\n',4,'https://upload.wikimedia.org/wikipedia/en/0/0e/BreathoftheWildFinalCover.jpg\n','Nintendo Switch; Nintendo Wii U'),(104,'Dynasty Warriors 8 Empires','v',30,'DYNASTY WARRIORS 8 Empires focuses deeply on the chaotic lives of the warriors as their fateful decisions affect the rise and fall of the nations they bravely fight for. Players can select from 83 unique characters, or create their own original character with a plentitude of options through the edit function. Whether choosing from Liu Bei, a provincial ruler forced to wander the land in search of a home or Lu Bu, an egotistical warrior who moves from one betrayal to another in search of his own glory, players are swept up in the drama of warfare. Courageous warriors trudge through Ancient China with its vast plains, bleak wastelands, narrow ravines and immense castles as they slash through enemy lines. In DYNASTY WARRIORS 8 Empires, warriors must fight against not only enemies but also the forces of nature as the changing seasons bring new elements to the more than 35 battlefields affected by seasons and the passing of time.',5,'https://images-na.ssl-images-amazon.com/images/I/91-ucF5DTzL._AC_SL1500_.jpg','PS4'),(105,'Batman Arkham Knight','v',16,'In the explosive finale to the Arkham series, Batman faces the ultimate threat against the city he is sworn to protect. The Scarecrow returns to unite an impressive roster of super villains, including Penguin, Two-Face and Harley Quinn, to destroy The Dark Knight forever. Batman: Arkham Knight introduces Rocksteadys uniquely designed version of the Batmobile, which is drivable for the first time in the franchise. The addition of this legendary vehicle, combined with the acclaimed gameplay of the Batman Arkham series, offers gamers the ultimate and complete Batman experience as they tear through the streets and soar across the skyline of the entirety of Gotham City. Be The Batman.',6,'https://upload.wikimedia.org/wikipedia/en/6/6c/Batman_Arkham_Knight_Cover_Art.jpg','PS4; Xbox One');
/*!40000 ALTER TABLE `video_games` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-19  8:58:28
